package tp3.courriel;

import java.util.ArrayList;

public class Courriel {
	
	private String adresseDest;
	private String titreMess;
	private String corpsMess;
	private ArrayList<String> piecesJointes;
	
	public Courriel(String adresseDest, String titreMess, String corpsMess, ArrayList<String> piecesJointes) {
		this.adresseDest = adresseDest;
		this.titreMess = titreMess;
		this.corpsMess = corpsMess;
		this.piecesJointes = piecesJointes;
	}

	public Courriel(String adresseDest, String titreMess, String corpsMess) {
		this.adresseDest = adresseDest;
		this.titreMess = titreMess;
		this.corpsMess = corpsMess;
	}
	
	//Pour plus de facilité durant les tests...
	public boolean titreNonVide() {
		return !titreMess.isEmpty();
	}
	
	public boolean adresseBienFormee() {
		return adresseDest.matches("(.+)@(.+)\\.(.+)");
	}
	
	public boolean envoyer() {
		if(this.adresseBienFormee() && this.titreNonVide()){
			if(corpsMess.contains("joint") || corpsMess.contains("PJ") || corpsMess.contains("jointe")) {
				if(!piecesJointes.isEmpty()) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		}
		return false;
	}
	
	public String toString() {
		return "Destinataire: "+this.adresseDest+" | Objet: "+this.titreMess+"\nCorps:\n"+this.corpsMess;
	}
	
}
