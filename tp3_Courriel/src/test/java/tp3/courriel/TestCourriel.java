package tp3.courriel;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TestCourriel {

	static Courriel c1;
	static ArrayList<String> pj;
	
	@BeforeAll
	public static void setUp() {
		pj = new ArrayList<String>();
		pj.add("/document/ilestla/truc.odt");
		c1 = new Courriel("test.truc@çafonctionne.gj", "MessageTop", "Je joint ce document oui oui blabla", pj);
	}
	
	
	@Test
	//Test de la méthode envoyer() avec un courriel bien formé contenant le mot "joint" et une piece jointe
	public void testEnvoi() {
		assertTrue(c1.envoyer());
	}
}
